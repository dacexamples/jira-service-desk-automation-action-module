# JIRA Service Desk - Automation Action Module example.

This add-on example shows you how you can add a custom automation action to JIRA Service Desk.

This example has a basic config form with one field.
And a webhook that simply logs the number of times it's called and prints out the information from the request.

We are using the ACE ([Atlassian Connect Express](https://bitbucket.org/atlassian/atlassian-connect-express)) framework. 

## Want to know more about adding a footer panel in the JIRA Service Desk Customer Portal?

[Adding your Footer Panel to the JIRA Service Desk Customer Portal](https://developer.atlassian.com/jiracloud/jira-service-desk-modules-customer-portal-39988271.html#JIRAServiceDeskmodules-Customerportal-Footerpanel).